# Movie Theater Database Management System
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
MOVIE TICKET BOOKING SYSTEM
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
SUMMARY
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
Movie Ticket Booking System is based on a concept of recording and booking customer’s movie ticket details. 
Here the user can perform all the tasks like booking a movie show, receiving tickets, and card registration and view all movie show details. 
The ticketer and the customer can create login accounts to book tickets. This project contains essentials features to book a movie ticket.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
RUNNING THE CODE
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
C++ based project to simulate the working of a movie theater reservation system.

The service simulates the following functionality for a 6 X 6 movie theater

Find the movies and the show timings for each movie. 
Allows new admins to create login accounts and existing admins to login using their existing login details. 
Allows new users to create login accounts and existing userss to login using their existing login details. 
Allows customers to purchase tickets. 
Allows the admin to create/delete/edit movie details. 

Code::Blocks is a free, open-source, cross-platform C, C++ and Fortran IDE built to meet the most demanding needs of its users. It is designed to be very extensible and fully configurable.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
EXECUTION STEPS
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
1) IF you dont have C++ and Code::Blocks installed:-

– Install Code::Blocks (mingw-setup) from http://www.codeblocks.org/downloads/binaries.
	* Note: Install the binary that has “mingw-setup”!
- Verify their installation 
– Create a new C++ file “hello.cpp” with the following code
       #include <iostream>
       int main(){
       std::cout << "Hello World!" << std::endl;
       return 0;
       }
– Build and run it using the top menu. You will see a new window with “Hello World!”

2) Navigate to the navigate to the project root folder and execute the following Movie Theater Booking System.cpp
3) Run and build the program 
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
FILES
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
1) Admin.txt: Stores all the admin login details like username and password. 
2) userid.txt: Stores all the admin login details like username and password. 
3) user.txt: Stores the user's login details and general detains like first name, last name, e-mail, card number, expiry date and CVV. 
4) Currentlyshowing.txt: Stores all the currently showing movie details like name and show timings. 
5) Movie Theater Booking System.cpp: Contains the C++ source code for the movie ticket booking system. 
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

